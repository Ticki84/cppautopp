/****************************** Module Header ******************************\
* Module Name:  Main.cpp
* Project:      CPPAutoPP
* Copyright (c) Microsoft Corporation & Florent CASPAR.
* 
* The code in Solution1.h/cpp demonstrates the use of #import to automate 
* PowerPoint. #import (http://msdn.microsoft.com/en-us/library/8etzzkb6.aspx), 
* a new directive that became available with Visual C++ 5.0, creates VC++ 
* "smart pointers" from a specified type library. It is very powerful, but 
* often not recommended because of reference-counting problems that typically 
* occur when used with the Microsoft Office applications. Unlike the direct 
* API approach in Solution2.h/cpp, smart pointers enable us to benefit from 
* the type info to early/late bind the object. #import takes care of adding 
* the messy guids to the project and the COM APIs are encapsulated in custom 
* classes that the #import directive generates.
* 
* This source is subject to the Microsoft Public License.
* See http://www.microsoft.com/en-us/openness/resources/licenses.aspx#MPL.
* All other rights reserved.
* 
* THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***************************************************************************/


#pragma region Includes
#include <stdio.h>
#include <windows.h>
#include <fstream>
#include <string>
#include <istream>
#include <streambuf>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <curl/curl.h>
#include <curl/easy.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/regex.hpp>
#include "Main.h"
#pragma endregion


#pragma region Namespace
namespace greg = boost::gregorian;
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::to_string;
using std::stoi;
using std::vector;
using boost::regex;
using boost::regex_replace;
using boost::replace_all;
using greg::date;
using greg::date_from_tm;
using greg::to_tm;
using greg::days;
#pragma endregion


#pragma region Import the type libraries

#import "libid:2DF8D04C-5BFA-101B-BDE5-00AA0044DE52" \
	rename("RGB", "MSORGB") \
	rename("DocumentProperties", "MSODocumentProperties")
// [-or-]
//#import "C:\\Program Files\\Common Files\\Microsoft Shared\\OFFICE12\\MSO.DLL" \
//	rename("RGB", "MSORGB") \
//	rename("DocumentProperties", "MSODocumentProperties")

using namespace Office;

#import "libid:0002E157-0000-0000-C000-000000000046"
// [-or-]
//#import "C:\\Program Files\\Common Files\\Microsoft Shared\\VBA\\VBA6\\VBE6EXT.OLB"

using namespace VBIDE;

#import "libid:91493440-5A91-11CF-8700-00AA0060263B" \
	rename("RGB", "VisioRGB")
// [-or-]
//#import "C:\\Program Files\\Microsoft Office\\Office12\\MSPPT.OLB" \
//	rename("RGB", "VisioRGB")

#pragma endregion


//
//   FUNCTION: GetModuleDirectory(LPWSTR, DWORD);
//
//   PURPOSE: This is a helper function in this sample. It retrieves the 
//      fully-qualified path for the directory that contains the executable 
//      file of the current process. For example, "D:\Samples\".
//
//   PARAMETERS:
//      * pszDir - A pointer to a buffer that receives the fully-qualified 
//      path for the directory taht contains the executable file of the 
//      current process. If the length of the path is less than the size that 
//      the nSize parameter specifies, the function succeeds and the path is 
//      returned as a null-terminated string.
//      * nSize - The size of the lpFilename buffer, in characters.
//
//   RETURN VALUE: If the function succeeds, the return value is the length 
//      of the string that is copied to the buffer, in characters, not 
//      including the terminating null character. If the buffer is too small 
//      to hold the directory name, the function returns 0 and sets the last 
//      error to ERROR_INSUFFICIENT_BUFFER. If the function fails, the return 
//      value is 0 (zero). To get extended error information, call 
//      GetLastError.
//
DWORD GetModuleDirectory(LPWSTR pszDir, DWORD nSize);




struct crenaux
{
	tm debut;
	tm fin;
	string resume;
	string salle;
	string matiere;
	string groupe;
	string prof;
};

bool crenaux_sorter(const crenaux& lhs, const crenaux& rhs)
{
	tm ltd = lhs.debut, rtd = rhs.debut, ltf = lhs.fin, rtf = rhs.fin;
	if (mktime(&ltd) != mktime(&rtd))
	{
		return mktime(&ltd) < mktime(&rtd);
	}
	if (mktime(&ltf) != mktime(&rtf))
	{
		return mktime(&ltf) > mktime(&rtf);
	}
	if (lhs.groupe != "" && rhs.groupe != "" && regex_replace(lhs.groupe, regex("[^0-9]*([0-9]+).*"), string("\\1")) != lhs.groupe && regex_replace(rhs.groupe, regex("[^0-9]*([0-9]+).*"), string("\\1")) != rhs.groupe && stoi(regex_replace(lhs.groupe, regex("[^0-9]*([0-9]+)"), string("\\1"))) != stoi(regex_replace(rhs.groupe, regex("[^0-9]*([0-9]+).*"), string("\\1"))))
	{
		return stoi(regex_replace(lhs.groupe, regex("[^0-9]*([0-9]+).*"), string("\\1"))) < stoi(regex_replace(rhs.groupe, regex("[^0-9]*([0-9]+).*"), string("\\1")));
	}
	return lhs.resume < rhs.resume;
}

string extraire(const string& chaine, const string debut, const string fin)
{
	if (chaine.empty()) { return string(); }
	if (chaine.find(debut) == string::npos && chaine.find(fin) == string::npos) { return chaine; }
	if (chaine.find(debut) == string::npos) { return chaine.substr(0, chaine.find(fin)); };
	auto start = chaine.find(debut) + debut.size();
	if (chaine.find(fin, start) == string::npos) { return chaine.substr(start); }
	auto resultat = chaine.substr(start, chaine.find(fin, start) - start);
	return resultat;
}

void erreur(const string& extrait, const string debut, const string fin, const string categorie, const bool erreur = true)
{
	auto start = extrait.find(debut);
	auto end = extrait.find(fin);
	if (start != string::npos)
	{
		cout << (erreur ? "Erreur: " : "Attention: ") << categorie << " non reconnu(e): " << extrait.substr(start, end - start);
	}
	else { cout << (erreur ? "Erreur: " : "Attention: ") << categorie << " introuvable."; }
}

void afficherTm(const tm& tim)
{
	cout << tim.tm_mday << "/" << tim.tm_mon + 1 << "/" << tim.tm_year + 1900 << " - " << tim.tm_hour << ":" << tim.tm_min << endl;
}

vector<crenaux> extraireParDate(vector<crenaux>& extraits, tm debut, tm fin)
{
	vector<crenaux> selection;
	for (vector<crenaux>::iterator it = extraits.begin(); it != extraits.end(); ++it)
	{
		if (crenaux_sorter(crenaux{ debut }, *it) && crenaux_sorter(*it, crenaux{ fin }))
		{
			selection.push_back(*it);
		}
	}
	return selection;
}

float duree(const crenaux& extrait)
{
	return (extrait.fin.tm_min - extrait.debut.tm_min + (extrait.fin.tm_hour - extrait.debut.tm_hour) * 60) / 30.f;
}

float dureePause(const crenaux& extraitl, const crenaux& extraitr)
{
	if (!crenaux_sorter(extraitl, extraitr)) { return 0.f; }
	return (extraitr.debut.tm_min - extraitl.fin.tm_min + (extraitr.debut.tm_hour - extraitl.fin.tm_hour) * 60) / 30.f;
}

int couleur(const crenaux& extrait)
{
	/////////////////////////////////////////////////////////////////////
	// D�claration des codes couleurs
	// 
	auto RGBFinal = RGB(192, 0, 0);
	auto RGBColle = RGB(124, 0, 0);
	auto RGBMaths = RGB(244, 128, 0);
	auto RGBPhysique = RGB(22, 134, 170);
	auto RGBChimie = RGB(30, 178, 226);
	auto RGBBio = RGB(125, 175, 45);
	auto RGBAnglais = RGB(55, 34, 77);
	auto RGBAllemand = RGB(83, 50, 115);
	auto RGBEspagnol = RGB(121, 74, 168);
	auto RGBInfo = RGB(135, 137, 139);
	auto RGBEPS = RGB(63, 87, 22);
	auto RGBEco = RGB(96, 96, 96);

	if (extrait.matiere == "Final") { return RGBFinal; }
	if (extrait.matiere == "Colle") { return RGBColle; }
	if (extrait.matiere == "Maths") { return RGBMaths; }
	if (extrait.matiere == "Physique") { return RGBPhysique; }
	if (extrait.matiere == "Chimie") { return RGBChimie; }
	if (extrait.matiere == "Bio") { return RGBBio; }
	if (extrait.matiere == "Anglais") { return RGBAnglais; }
	if (extrait.matiere == "Allemand") { return RGBAllemand; }
	if (extrait.matiere == "Espagnol") { return RGBEspagnol; }
	if (extrait.matiere == "Info") { return RGBInfo; }
	if (extrait.matiere == "EPS") { return RGBEPS; }
	if (extrait.matiere == "Eco") { return RGBEco; }
	return RGB(64, 64, 64);
}

vector<int> creneauxSimultanes(const vector<crenaux>& extraits)
{
	vector<int> nbSimultanes;
	for (int i = 0; i < (int)extraits.size(); i++)
	{
		tm ide = extraits[i].debut, ifi = extraits[i].fin;
		int p = 0;
		for (int j = 0; j < (int)extraits.size(); j++)
		{
			tm jde = extraits[j].debut, jfi = extraits[j].fin;
			if (mktime(&ide) < mktime(&jfi) && mktime(&ifi) > mktime(&jde))
				p++;
		}
		nbSimultanes.push_back(p);
	}
	return nbSimultanes;
}

vector<int> positionCrenaux(vector<int>& nbSimultanes)
{
	vector<int> position;
	for (int i = 0; i < (int)nbSimultanes.size();)
	{
		if (nbSimultanes[i] != nbSimultanes[i + nbSimultanes[i] - 1])
		{
			position.push_back(0);
			for (int j = 1; j < nbSimultanes[i];)
			{
				for (int k = 1; k < nbSimultanes[j]; k++)
				{
					position.push_back(k);
				}
				j += nbSimultanes[j] - 1;
			}
			nbSimultanes[i] -= 1;
			i += nbSimultanes[i] + 1;
		}
		else
		{
			for (int j = 0; j < nbSimultanes[i]; j++)
			{
				position.push_back(j);
			}
			i += nbSimultanes[i];
		}
	}
	return position;
}

string date_str(const date& extrait)
{
	string d = string(2 - (int)to_string(extrait.day().as_number()).size(), '0') + to_string(extrait.day().as_number());
	if (extrait.month() == greg::Jan) { return d + " Jan"; }
	else if (extrait.month() == greg::Feb) { return d + " F�v"; }
	else if (extrait.month() == greg::Mar) { return d + " Mars"; }
	else if (extrait.month() == greg::Apr) { return d + " Avr"; }
	else if (extrait.month() == greg::May) { return d + " Mai"; }
	else if (extrait.month() == greg::Jun) { return d + " Juin"; }
	else if (extrait.month() == greg::Jul) { return d + " Juil"; }
	else if (extrait.month() == greg::Aug) { return d + " Ao�t"; }
	else if (extrait.month() == greg::Sep) { return d + " Sep"; }
	else if (extrait.month() == greg::Oct) { return d + " Oct"; }
	else if (extrait.month() == greg::Nov) { return d + " Nov"; }
	else { return d + " D�c"; }
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

static size_t write_string(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

vector<crenaux> extraireTout(const int annee, const bool proxyUL, const bool utiliserFichier)
{
	vector<crenaux> listeCours;

	if (!utiliserFichier)
		cout << "T\202l\202chargement du calendrier des " << annee << "A" << endl;

	string url = "https://planning.univ-lorraine.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=111,109076,109113,150808,150809,150810,150811,150812,150813,150814,150815,150816,150817,150818,150819,150820,150821,1496,32844,32845,32846,32847,109115,109116,150824,150825,150826,150827,150828,150829,150830,150831,150832,150833,150834,150835,150836,150837,150838,229386,229387,229388,31604,31606,31660,32848,32849,32850,32855,150959,150960,150970,150975,150981,150982,150983,150984,150985,150986,150987,150988,151016,151018,1498,150869,150873,1503,150876,150877,150878,150879,150880,150881,150882,150883,150884,221340,150888,150889,150890,150891,63917,150893,150894,150901,31591,150903,150905,150906,150907,150908,150909,31593,31595,31597,150930,150931,150932,150933,150934,150935,150936,150937,150938,150939,150940,150941,150944,150945,31600,31602,150948,150949,150950,150951,31721,151100,151476,151477,151506,151511,151553,31825,151556,151572,151686,151687,151688,151689,151693,151694,151695,151696,63918,151698,151699,151700,31831,151702,151703,32047,151706,151707,32071,151710,151711,151712,151713,151714,151715,150912,150913,150915,150917,150918,150920,150922,150924,150926,151718,151719,151721,151723,151724,151731,151733,151735,151737&projectId=6&calType=ical&nbWeeks=16";
	if (annee == 2)
		url = "https://planning.univ-lorraine.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources=239199,151746,151747,151748,151750,151751,151752,151753,151754,109900,109901,109902,151757,151758,151759,151760,151761,226297,226298,151764,151765,151766,151767,151768,151769,151770,151772,151773,151774,151790,151791,151833,151834,151857,151858,151859,151862,151863,151864,151866,151873,151874,152489,152490,152506,152874,152875,152876,153157,16311,153159,153160,153162,153197,153198,160814,160816,160817,160819,160821,152634,152635,152637,152640,152659,152779,152788,152794,152870,153257,153258,153592,153596,153597,153636,154091,154093,154095,155751,155752,155756,154107,154108,57870,57871,91308,152701,152702,154112,154113,154114,154115,154116,154117,154118,165893,154120,154121,154122,154527,154528,154971,154986,155099,155100,155102,155103,155109,155110,155320,155471,155472,155473,155521,155522,155524,155525,155527,155530,155747&projectId=6&calType=ical&nbWeeks=16";

	CURL* curl = curl_easy_init();
	if (curl)
	{
		string contenu;
		if (!utiliserFichier)
		{
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			if (proxyUL)
			{
				curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy.infra.univ-lorraine.fr:3128");
				curl_easy_setopt(curl, CURLOPT_PORT, 443L);
			}
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
			curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "deflate");
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_string);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &contenu);
			CURLcode res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			if (res != CURLE_OK) {
				fprintf(stderr, "curl_easy_perform() a \202chou\202: %s\n",
					curl_easy_strerror(res));
				cin.ignore();
			}
		}
		else
		{
			std::ifstream fichierCal;
			fichierCal.open("ADECal.ics");
			std::stringstream strStream;
			strStream << fichierCal.rdbuf();
			contenu = strStream.str();
		}
		replace_all(contenu, regex("\r"), "");

		std::wcout << "Lecture du calendrier des " << annee << "A" << endl;

		vector<string> extraits;
		auto start = contenu.find("DTSTART:");
		auto end = contenu.find("UID:");
		while (end != string::npos)
		{
			extraits.push_back(contenu.substr(start, end - start));
			int idx = (int)extraits.size() - 1;
			crenaux extrait;

			boost::smatch sm;
			// D�but
			if (boost::regex_search(extraits[idx], sm, regex("DTSTART:([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})Z")))
			{
				extrait.debut = { stoi(sm[6]), stoi(sm[5]), stoi(sm[4]), stoi(sm[3]), stoi(sm[2]) - 1, stoi(sm[1]) - 1900 };
			}
			else { erreur(extraits[idx], "DTSTART:", "DTEND:", "d\202but"); }
			// Fin
			if (boost::regex_search(extraits[idx], sm, regex("DTEND:([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})Z")))
			{
				extrait.fin = { stoi(sm[6]), stoi(sm[5]), stoi(sm[4]), stoi(sm[3]), stoi(sm[2]) - 1, stoi(sm[1]) - 1900 };
			}
			else { erreur(extraits[idx], "DTEND:", "SUMMARY:", "fin"); }
			// Salle
			if (boost::regex_search(extraits[idx], sm, regex("LOCATION:(.*)\nDESCRIPTION:")))
			{
				extrait.salle = sm[1];
				replace_all(extrait.salle, regex("Roubault_"), "");
				replace_all(extrait.salle, regex("\\,"), "~");
			}
			else { erreur(extraits[idx], "LOCATION:", "DESCRIPTION:", "salle"); }
			// Groupe
			if (boost::regex_search(extraits[idx], sm, regex("SUMMARY:.*(?:(CM)|(TD[0-9]?)|(Soutien)|(TP[0-9])|(TP groupe [0-9](?:\.[0-9])?)|(Final(?: n� [0-9])?)|(Colle [0-9]+)|(EC) |(Présentation) |(Visite)|Journée des [Aa]nciens|(Exposés [0-9])|(Roches [0-9])|(Tutorat [0-9]+)|(?:([Gg]roupe [0-9])[^.])).*\nLOCATION:")))
			{
				for (int i = 1; i < (int)sm.size(); i++)
				{
					if (sm[i] != "")
					{
						extrait.groupe = sm[i];
						replace_all(extrait.groupe, regex("groupe "), "GRP");
						replace_all(extrait.groupe, regex("Groupe "), "GRP");
						replace_all(extrait.groupe, regex("Tutorat "), "TUT");
						replace_all(extrait.groupe, regex("Colle "), "TUT");
						replace_all(extrait.groupe, regex("n� "), "");
						break;
					}
				}
				if (extraits[idx].find("Thème") != string::npos && extraits[idx].find("S4") != string::npos)
				{
					if (boost::regex_search(extraits[idx], sm, regex("SUMMARY:.*Thème (?:(Méca)|(Bio)|(TI2E)|(Phys))")))
					{
						if ((int)sm.size() >= 2 && sm[1] != "")
						{
							extrait.groupe = extrait.groupe + " ~ Th�me";
						}
					}
				}
				replace_all(extrait.groupe, "É", "�");
				replace_all(extrait.groupe, "é", "�");
				replace_all(extrait.groupe, "è", "�");
				replace_all(extrait.groupe, "ê", "�");
				if (extrait.groupe == "Pr�sentation")
					extrait.groupe = "Pr�s�";
			}
			else { erreur(extraits[idx], "SUMMARY:", "LOCATION:", "groupe", false); }


			// R�sum�
			if (boost::regex_search(extraits[idx], sm, regex("SUMMARY:.*(?:(?<!Colle [0-9] )(?<!Colle [0-9]{2} )Physique ([A-Z]+[0-9])[ :]|Colle [0-9]+ (Physique)|(Physique).+EC |(Calcul).+[Pp]hysique|: (Prépa Stage)\nLOCATION|(EPS)|(Maths)|(Anglais)|(Allemand)|(Espagnol)|(Tutorat)|(?:Physique : (?:Soutien )?([A-Z]{1,2}[0-9]))|(Géo.+)\nLOCATION|(Sciences éco)|(Sciences Eco)|(?:Final ([A-Z]{1,2}[0-9]))|Présentation (.+)\nLOCATION|(Chimie.+?)\nLOCATION|(Informatique)|(Info)[^r]|Visite école (.+)\nLOCATION|(Journée des [Aa]nciens)|(Bio.+)(?<!Exposés [0-9])(?<!Roches [0-9])\nLOCATION|(Bio.+)(?: Exposés [0-9]| Roches [0-9])\nLOCATION|(Accueil Réunionnais - Valence)|(Journée Portes Ouvertes)|(Relativité)|(?:(Méca)(?:nique)?(?: (?:(Mil).* (Cont)|(Flu)|(Quan)|(CAO)))?.*)\nLOCATION|(?:Thème (?:(Phys)|(PC)))|(?:Thème (TI2E)|(?:Thème (Bio(?:logie)? T[0-9])))|(Conseil S[1-4]))")))
			{
				for (int i = 1; i < (int)sm.size(); i++)
				{
					if (sm[i] != "")
					{
						extrait.resume = sm[i];
						break;
					}
				}
			}
			else
			{
				erreur(extraits[idx], "SUMMARY:", "LOCATION:", "r\202sum\202", false);
				auto dbis = extraits[idx].find("SUMMARY:");
				auto fbis = extraits[idx].find("\nLOCATION:");
				if (dbis != string::npos && fbis != string::npos) { extrait.resume = extraits[idx].substr(dbis, fbis - dbis); }
			}
			replace_all(extrait.resume, "SUMMARY:", "");
			replace_all(extrait.resume, "É", "�");
			replace_all(extrait.resume, "é", "�");
			replace_all(extrait.resume, "è", "�");
			replace_all(extrait.resume, "ê", "�");
			if (extrait.resume == "Sciences �co" || extrait.resume == "Sciences Eco")
				extrait.resume = "Sciences �co.";
			else if (extrait.resume == "Calcul")
				extrait.resume = "Calcul Physique";
			else if (extrait.resume == "Phys")
				extrait.resume = "PC";
			else if (extrait.resume == "Info")
				extrait.resume = "Informatique";

			// Professeur
			string pes = extraire(extraits[idx], "DESCRIPTION:", "(Mod");
			replace_all(pes, "\n ", "");
			replace_all(pes, "\n", "");
			replace_all(pes, "\\n", ";");
			if (boost::regex_search(pes, sm, regex(";((?:[A-Z]+(?<!CM)[ -])+(?:[A-Z][a-zéèê]+[ -]?)+)")))
			{
				string nomComplet = sm[1];
				replace_all(nomComplet, " ", ";");
				replace_all(nomComplet, "-", ";");
				if (boost::regex_search(nomComplet, sm, regex(";([A-Z])[a-zéèê]+(?:;([A-Z])[a-zéèê]+)?(?:;([A-Z])[a-zéèê]+)?(?:;([A-Z])[a-zéèê]+)?(?:;([A-Z])[a-zéèê]+)?")))
				{
					for (int i = 1; i < (int)sm.size(); i++)
					{
						if (sm[i] != "") { extrait.prof += sm[i]; }
					}
				}
				if (boost::regex_search(nomComplet, sm, regex("([A-Z])[A-Z]+(?:;([A-Z])[A-Z]+)?(?:;([A-Z])[A-Z]+)?(?:;([A-Z])[A-Z]+)?(?:;([A-Z])[A-Z]+)?")))
				{
					for (int i = 1; i < (int)sm.size(); i++)
					{
						if (sm[i] != "") { extrait.prof += sm[i]; }
					}
				}
			}
// 			if (boost::regex_search(extraits[idx], sm, regex("SUMMARY:.+[^:] ([A-Z]{2,5}(?<!EPS))\nLOCATION:")))
// 			{
// 				extrait.prof = sm[1];
// 			}

			// Mati�re
			string res = extraire(extraits[idx], "SUMMARY:", string(1, '\n'));
			if (res.find("Final") != string::npos || res.find("EC ") != string::npos) { extrait.matiere = "Final"; }
			else if (res.find("Colle") != string::npos) { extrait.matiere = "Colle"; }
			else if (res.find("Maths") != string::npos) { extrait.matiere = "Maths"; }
			else if (res.find("Physique") != string::npos) { extrait.matiere = "Physique"; }
			else if (res.find("Chimie") != std::string::npos) { extrait.matiere = "Chimie"; }
			else if (res.find("Bio") != string::npos || res.find("Géo") != string::npos) { extrait.matiere = "Bio"; }
			else if (res.find("Anglais") != string::npos) { extrait.matiere = "Anglais"; }
			else if (res.find("Allemand") != string::npos) { extrait.matiere = "Allemand"; }
			else if (res.find("Espagnol") != string::npos) { extrait.matiere = "Espagnol"; }
			else if (res.find("Informatique") != string::npos) { extrait.matiere = "Info"; }
			else if (res.find("EPS") != string::npos) { extrait.matiere = "EPS"; }
			else if (res.find("Eco") != string::npos || res.find("Stage") != string::npos) { extrait.matiere = "Eco"; }

			if (extrait.matiere == "Colle") { extrait.resume = "Colle " + extrait.resume; }
			else if (extrait.resume != "Tutorat")
			{
				if (extrait.prof == "LB") { extrait.resume = "Analyse"; }
				else if (extrait.prof == "VL") { extrait.resume = "Analyse"; }
				else if (extrait.prof == "AP") { extrait.resume = "Alg�bre"; }
			}

			if (extraits[idx].find("DESCRIPTION:\\n\\n\\n\\n") == string::npos)
				listeCours.push_back(extrait);
			start = contenu.find("DTSTART:", end);
			end = contenu.find("UID:", start);
		}
		cout << listeCours.size() << " cr\202naux ont \202t\202 import\202" << endl;
	}
	else
	{
		cout << "Erreur: connexion impossible" << endl;
		cin.ignore();
	}
	return listeCours;
}



//
//   FUNCTION: AutomatePowerPointByImport(LPVOID)
//
//   PURPOSE: Automate Microsoft PowerPoint using the #import directive and 
//      smart pointers.
// 
DWORD WINAPI AutomatePowerPointByImport(LPVOID lpParam)
{
	// Initializes the COM library on the current thread and identifies the 
	// concurrency model as single-thread apartment (STA). 
	// [-or-] CoInitialize(NULL);
	// [-or-] CoCreateInstance(NULL);
	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

	try
	{


		bool utiliserFichier = false;
		bool proxyUL = false;
		int ajouterJours = 0;

		cout << "CPPAutoPP  ---  Florent CASPAR" << endl << endl;

		// Obtention du chemin de l'ex�cutable
		wchar_t repertoire[MAX_PATH], fichierExe[MAX_PATH], fichierTmp[MAX_PATH], fichierDiapo[MAX_PATH];
		if (!GetModuleDirectory(repertoire, ARRAYSIZE(repertoire)))
		{
			cout << "L'obtention du r\202pertoire a \202chou\202" << endl;
			cin.ignore();
			return 1;
		}
		wcsncpy_s(fichierExe, repertoire, MAX_PATH);
		wcsncpy_s(fichierTmp, repertoire, MAX_PATH);
		wcsncpy_s(fichierDiapo, repertoire, MAX_PATH);
		wcsncat_s(fichierExe, ARRAYSIZE(fichierExe), L"CPPAutoPP.exe", 12);
		wcsncat_s(fichierTmp, ARRAYSIZE(fichierTmp), L"CPPAutoPP.old.exe", 16);
		wcsncat_s(fichierDiapo, ARRAYSIZE(fichierDiapo), L"EDT.pptx", 12);

		remove(_bstr_t(fichierTmp));


		/////////////////////////////////////////////////////////////////////
		// T�l�chargement des fichiers n�cessaires
		// 

		// Version
		cout << "V\202rification de la version et d\202tection automatique du proxy" << endl;
	
		string url = "https://gitlab.com/api/v4/projects/8744677/repository/files/Autres%2FCPPAutoPP%2Exml/raw?ref=master&private_token=gc_UmrPe8zw3BcpvhZcS";

		CURL* curl = curl_easy_init();
		if (curl)
		{
			string result;
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			if (proxyUL)
			{
				curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy.infra.univ-lorraine.fr:3128");
				curl_easy_setopt(curl, CURLOPT_PORT, 443L);
			}
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
			curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "deflate");
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_string);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
			CURLcode res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			if (res != CURLE_OK) {
				proxyUL = false;
				result = string();
				curl = curl_easy_init();
				curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
				curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
				curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "deflate");
				curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
				curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_string);
				curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
				CURLcode res2 = curl_easy_perform(curl);
				curl_easy_cleanup(curl);
				if (res2 != CURLE_OK) {
					fprintf(stderr, "curl_easy_perform() a \202chou\202: %s\n",
						curl_easy_strerror(res));
					fprintf(stderr, "curl_easy_perform() a \202chou\202 (avec proxy): %s\n",
						curl_easy_strerror(res2));
					cin.ignore();
					return 1;
				}
			}

			if (proxyUL) cout << "Connexion via le proxy UL" << endl;
			else cout << "Connexion sans proxy" << endl;

			boost::smatch sm;
			if (boost::regex_search(result, sm, regex("<version>([0-9]+)\.([0-9]+)\.([0-9]+)</version>")))
			{
				int maVersion[3] = { 0, 0, 4 };
				if (stoi(sm[1]) > maVersion[0] || (stoi(sm[1]) == maVersion[0] && stoi(sm[2]) > maVersion[1]) || (stoi(sm[1]) == maVersion[0] && stoi(sm[2]) == maVersion[1] && stoi(sm[3]) > maVersion[2]))
				{
					// Ex�cutable
					cout << "T\202l\202chargement de la derni\212re version (" << sm[1] << "." << sm[2] << "." << sm[3] << ")" << endl;
				
					string url = "https://gitlab.com/api/v4/projects/8744677/repository/files/Release%2FCPPAutoPP%2Eexe/raw?ref=master&private_token=gc_UmrPe8zw3BcpvhZcS";

					CURL* curl = curl_easy_init();
					if (curl)
					{
						rename(_bstr_t(fichierExe), _bstr_t(fichierTmp));
						FILE* fp;
						fopen_s(&fp, _bstr_t(fichierExe), "wb");
						curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
						if (proxyUL)
						{
							curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy.infra.univ-lorraine.fr:3128");
							curl_easy_setopt(curl, CURLOPT_PORT, 443L);
						}
						curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
						curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
						curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
						CURLcode res = curl_easy_perform(curl);
						curl_easy_cleanup(curl);
						fclose(fp);
						if (res != CURLE_OK) {
							rename(_bstr_t(fichierTmp), _bstr_t(fichierExe));
							fprintf(stderr, "curl_easy_perform() a \202chou\202: %s\n",
								curl_easy_strerror(res));
							cin.ignore();
							return 1;
						}
						cout << "Programme t\202l\202charg\202 avec succ\212s" << endl << "Red\202marrage" << endl;

						STARTUPINFO siStartupInfo;
						PROCESS_INFORMATION piProcessInfo;
						memset(&siStartupInfo, 0, sizeof(siStartupInfo));
						memset(&piProcessInfo, 0, sizeof(piProcessInfo));
						siStartupInfo.cb = sizeof(siStartupInfo);

						::CreateProcess(fichierExe, // application name/path
							NULL, // command line (optional)
							NULL, // no process attributes (default)
							NULL, // default security attributes
							false,
							CREATE_DEFAULT_ERROR_MODE | CREATE_NEW_CONSOLE,
							NULL, // default env
							NULL, // default working dir
							&siStartupInfo,
							&piProcessInfo);

						::TerminateProcess(GetCurrentProcess(), 0);
						::ExitProcess(0);
					}
					else
					{
						cout << "Erreur: connexion impossible" << endl;
						cin.ignore();
						return 1;
					}
				}
				else
					cout << "Vous poss\202dez la derni\212re version (" << maVersion[0] << "." << maVersion[1] << "." << maVersion[2] << ")" << endl;
			}
		}
		else
		{
			cout << "Erreur: connexion impossible" << endl;
			cin.ignore();
			return 1;
		}


		// Mod�le
		cout << "T\202l\202chargement du mod\212le" << endl;

		url = "https://gitlab.com/api/v4/projects/8744677/repository/files/Autres%2FSlide%2Epptx/raw?ref=master&private_token=gc_UmrPe8zw3BcpvhZcS";

		curl = curl_easy_init();
		if (curl)
		{
			FILE* fp;
			fopen_s(&fp, _bstr_t(fichierDiapo), "wb");
			curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
			if (proxyUL)
			{
				curl_easy_setopt(curl, CURLOPT_PROXY, "http://proxy.infra.univ-lorraine.fr:3128");
				curl_easy_setopt(curl, CURLOPT_PORT, 443L);
			}
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
			CURLcode res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);
			fclose(fp);
			if (res != CURLE_OK) {
				fprintf(stderr, "curl_easy_perform() a \202chou\202: %s\n",
					curl_easy_strerror(res));
				cin.ignore();
				return 1;
			}
			cout << "Mod\212le t\202l\202charg\202 avec succ\212s" << endl;
		}
		else
		{
			cout << "Erreur: connexion impossible" << endl;
			cin.ignore();
			return 1;
		}


		/////////////////////////////////////////////////////////////////////
		// Cr�e l'objet COM PowerPoint.Application en utilisant des
		// directives #include et des pointeurs intelligents
		// 
		cout << "Lancement de PowerPoint" << endl;

		PowerPoint::_ApplicationPtr spPpApp;
		HRESULT hr = spPpApp.CreateInstance(__uuidof(PowerPoint::Application));
		if (FAILED(hr))
		{
			wprintf(L"CreateInstance failed w/err 0x%08lx\n", hr);
			cin.ignore();
			return 1;
		}


		/////////////////////////////////////////////////////////////////////
		// Chargement du diaporma et des diapositives
		// 

		PowerPoint::PresentationsPtr spPres = spPpApp->Presentations;
		PowerPoint::_PresentationPtr spPre = spPres->Open(_bstr_t(fichierDiapo), Office::msoTrue, Office::msoTrue, Office::msoTrue);
		PowerPoint::SlidesPtr spSlides = spPre->Slides;
		PowerPoint::_SlidePtr spSlide;


		/////////////////////////////////////////////////////////////////////
		// Extraction des donn�es et g�n�ration des vecteurs n�cessaires
		// 
		cout << "T\202l\202chargement et extraction des donn\202es" << endl;

		float margeGauche = 65.26f; // en pt ~ 48.00f:65.26f
		float margeHaut = 73.13f;
		float hCrenaux = 77.67f;
		float lCrenaux = 40.61f; // =30 minutes ~ 30.61f:40.61f

		// R�cup�ration des dates n�cessaires
		tm timeinfo;
		time_t t = time(0);
		localtime_s(&timeinfo, &t);
		date actuelle(date_from_tm(timeinfo));
		actuelle += days(ajouterJours);


		/////////////////////////////////////////////////////////////////////
		// Traitement des diff�rentes pages
		// 
		for (int page = 1; page <= (utiliserFichier ? 1 : 2); page++)
		{
			// S�lection et affichage de la diapositive correspondante
			spSlide = spSlides->FindBySlideID((long)(256 + page));
			spPpApp->ActiveWindow->View->PutSlide(spSlide);

			// Extraction des donn�es
			vector<crenaux> extraits = extraireTout(page, proxyUL, utiliserFichier);
			vector<crenaux> semaine = extraireParDate(extraits, to_tm(actuelle), to_tm(actuelle + days(7)));

			// Tri des donn�es
			sort(semaine.begin(), semaine.end(), &crenaux_sorter);
			vector<vector<crenaux>> jours;
			vector<vector<int>> uniques;
			vector<vector<int>> positions;
			int k = 0;
			for (int j = 0; j < 7; j++)
			{
				jours.push_back(extraireParDate(semaine, to_tm(actuelle + days(j)), to_tm(actuelle + days(j + 1))));
				uniques.push_back(creneauxSimultanes(jours[k]));
				positions.push_back(positionCrenaux(uniques[k]));
				for (std::vector<int>::const_iterator i = uniques[k].begin(); i != uniques[k].end(); ++i)
					std::cout << *i << ' ';
				cout << endl;
				for (std::vector<int>::const_iterator i = positions[k].begin(); i != positions[k].end(); ++i)
					std::cout << *i << ' ';
				cout << endl;
				if (!jours[k].empty())
					for (int h = 0; h < (int)jours[k].size() - 1; h++)
					{
						if (uniques[k][h] == 1 && uniques[k][h + 1] == 1 && jours[k][h].resume == jours[k][h + 1].resume && jours[k][h].salle == jours[k][h + 1].salle && jours[k][h].matiere == jours[k][h + 1].matiere && jours[k][h].groupe == jours[k][h + 1].groupe && jours[k][h].prof == jours[k][h + 1].prof)
						{
							crenaux crenauxA = jours[k][h];
							crenaux crenauxB = jours[k][h + 1];
							if (mktime(&crenauxA.fin) == mktime(&crenauxB.debut))
							{
								jours[k][h].fin = jours[k][h + 1].fin;
								jours[k].erase(jours[k].begin() + h + 1);
								uniques[k].erase(uniques[k].begin() + h + 1);
								positions[k].erase(positions[k].begin() + h + 1);
							}
						}
					}
					cout << jours[k].size() << " cr\202naux le " << actuelle + days(j) << endl;
				k++;
			}


			/////////////////////////////////////////////////////////////////////
			// G�n�ration du diaporama
			// 
			cout << "G\202n\202ration du diaporama des " << page << "A" << endl;

			PowerPoint::ShapesPtr spShapes = spSlide->Shapes;


			// G�n�ration des jours
			int ridx = actuelle.day_of_week().as_number();
			for (int i = 0; i < 7; i++)
			{
				if (ridx != 0)
				{
					PowerPoint::ShapePtr spShape = spShapes->AddTextbox(Office::msoTextOrientationHorizontal, 3.12f, margeHaut + (ridx - 1) * hCrenaux + hCrenaux / 2.f - 3.f, 50.f, 22.4f);
					spShape->TextFrame->TextRange->Text = _bstr_t(date_str(actuelle + days(i)).c_str());
					spShape->TextFrame->TextRange->Font->Size = 12.f;
					spShape->TextFrame->TextRange->Font->Color->PutVisioRGB(RGB(77, 77, 77));
					spShape->TextFrame->TextRange->Font->Name = "Impact";
				}
				ridx++;
				if (ridx > 6)
					ridx = 0;
			}

			// G�n�ration de la barre du jour actuelle
			ridx = actuelle.day_of_week().as_number();
			if (ridx != 0)
			{
				// 720:960
				PowerPoint::ShapePtr rect = spShapes->AddShape(Office::msoShapeRectangle, 0, margeHaut + (ridx - 1) * hCrenaux, 960, hCrenaux);
				rect->Fill->ForeColor->PutVisioRGB(RGB(22, 134, 170));
				rect->Fill->PutTransparency(0.7f);
				rect->Line->PutVisible(Office::msoFalse);
			}

			// G�n�ration des cr�naux
			for (int j = 0; j < 7; j++)
			{
				if (!jours[j].empty())
				{
					float margeGaucheTot = margeGauche + lCrenaux * (dureePause(crenaux{ { 0, 0, 7 }, { 0, 0, 7 } }, jours[j][0])); // Attention heure d'�t�

//					int n = 0;
					for (int k = 0; k < (int)uniques[j].size(); k++)
					{
						if (k > 0)
							margeGaucheTot += lCrenaux * dureePause(jours[j][k - 1], jours[j][k]);

						float dureeC = duree(jours[j][k]);
						float hauteurC = uniques[j][k] == 1 ? 50.f : hCrenaux / uniques[j][k];
						float taillePolice = max(6.f, (14.f - 1.5f * uniques[j][k] - (((int)jours[j][k].resume.size() > 12 && uniques[j][k] < 3) ? 0.12f * jours[j][k].resume.size() : 0)) * (dureeC < 3.f ? dureeC / 3.2f : 1));
						float margeHautC = margeHaut + (ridx - 1) * hCrenaux + ((uniques[j][k] == 1) ? (hCrenaux - 50.f) / 2.f : hCrenaux * positions[j][k] / uniques[j][k]);
						PowerPoint::ShapePtr rect = spShapes->AddShape(Office::msoShapeRectangle, margeGaucheTot, margeHautC, lCrenaux * dureeC, uniques[j][k] == 1 ? 50.f : hCrenaux / uniques[j][k]);
						rect->Fill->ForeColor->PutVisioRGB(couleur(jours[j][k]));
						rect->Line->PutVisible(Office::msoFalse);
						PowerPoint::ShapePtr spResume = spShapes->AddTextbox(Office::msoTextOrientationHorizontal, margeGaucheTot, margeHautC - 0.71f * uniques[j][k], lCrenaux * dureeC, hauteurC);
						spResume->TextFrame->TextRange->Text = _bstr_t((jours[j][k].resume).c_str());
						spResume->TextFrame->TextRange->Font->Size = taillePolice;
						spResume->TextFrame->TextRange->Font->Color->PutVisioRGB(RGB(255, 255, 255));
						spResume->TextFrame->TextRange->Font->Name = "Helvetica";
						spResume->TextFrame->TextRange->Font->PutBold(Office::msoTrue);
						if (jours[j][k].prof != "")
						{
							PowerPoint::ShapePtr spProf = spShapes->AddTextbox(Office::msoTextOrientationHorizontal, margeGaucheTot, margeHautC - 0.71f * uniques[j][k], lCrenaux * dureeC, hauteurC);
							spProf->TextFrame->TextRange->Text = _bstr_t((jours[j][k].prof).c_str());
							spProf->TextFrame->TextRange->Font->Size = taillePolice;
							spProf->TextFrame->TextRange->Font->Color->PutVisioRGB(RGB(255, 255, 255));
							spProf->TextFrame->TextRange->Font->Name = "Helvetica";
							spProf->TextFrame->TextRange->Font->PutItalic(Office::msoTrue);
							spProf->TextFrame->TextRange->ParagraphFormat->PutAlignment(PowerPoint::ppAlignRight);
						}
						PowerPoint::ShapePtr spGroupe = spShapes->AddTextbox(Office::msoTextOrientationHorizontal, margeGaucheTot, margeHautC - 0.71f * uniques[j][k] + hauteurC / 2.f, lCrenaux * dureeC, hauteurC);
						spGroupe->TextFrame->TextRange->Text = _bstr_t((jours[j][k].groupe).c_str());
						spGroupe->TextFrame->TextRange->Font->Size = taillePolice;
						spGroupe->TextFrame->TextRange->Font->Color->PutVisioRGB(RGB(255, 255, 255));
						spGroupe->TextFrame->TextRange->Font->Name = "Helvetica";
						if (jours[j][k].salle != "")
						{
							PowerPoint::ShapePtr spSalle = spShapes->AddTextbox(Office::msoTextOrientationHorizontal, margeGaucheTot, margeHautC - 0.71f * uniques[j][k] + hauteurC / 2.f, lCrenaux * dureeC, hauteurC);
							spSalle->TextFrame->TextRange->Text = _bstr_t((jours[j][k].salle).c_str());
							spSalle->TextFrame->TextRange->Font->Size = taillePolice;
							spSalle->TextFrame->TextRange->Font->Color->PutVisioRGB(RGB(255, 255, 255));
							spSalle->TextFrame->TextRange->Font->Name = "Helvetica";
							spSalle->TextFrame->TextRange->ParagraphFormat->PutAlignment(PowerPoint::ppAlignRight);
						}
// 						PowerPoint::ShapePtr spShape = spShapes->AddTextbox(Office::msoTextOrientationHorizontal, margeGaucheTot, margeHaut + (ridx - 1) * hCrenaux + ((uniques[j][k] == 1) ? (hCrenaux - 50.f) / 2.f : hCrenaux * n / uniques[j][k]), 0.f, 0.f);
// 						spShape->Fill->ForeColor->PutVisioRGB(couleur(jours[j][k]));
// 						spShape->TextFrame->TextRange->Text = _bstr_t((jours[j][k].resume + '\n' + jours[j][k].groupe + string(max(1, (int)(8 * duree(jours[j][k]) / 3.f - jours[j][k].groupe.size() - jours[j][k].salle.size() + 3.0f * uniques[j][k])), ' ') + jours[j][k].salle).c_str());
// 						spShape->TextFrame->TextRange->Font->Size = max(6.f, (14.f - 1.5f * uniques[j][k] - (((int)jours[j][k].resume.size() > 12 && uniques[j][k] < 3) ? 0.15f * jours[j][k].resume.size() : 0)) * (duree(jours[j][k]) < 3.f ? duree(jours[j][k]) / 3.f : 1));
// //						spShape->TextFrame->TextRange->Font->PutBold(Office::msoTrue);
// 						spShape->TextFrame->TextRange->Font->Color->PutVisioRGB(RGB(255, 255, 255));
// 						spShape->TextFrame->TextRange->Font->Name = "Helvetica";
// 						spShape->TextFrame->AutoSize = PowerPoint::ppAutoSizeNone;
// 						spShape->TextFrame->VerticalAnchor = Office::msoAnchorMiddle;
// // 						spShape->ThreeD->PutBevelTopType(Office::msoBevelCircle);
// // 						spShape->ThreeD->PutLightAngle(145.f);
// // 						spShape->ThreeD->PutBevelTopInset(11.f + 1.f * uniques[j][k]);
// // 						spShape->ThreeD->PutBevelTopDepth(3.f);
// // 						spShape->ThreeD->PutPresetLighting(Office::msoLightRigBalanced);
// // 						spShape->Shadow->PutVisible(Office::msoTrue);
// // 						spShape->Shadow->PutBlur(3.5f);
// // 						spShape->Shadow->PutTransparency(0.68f);
// // 						spShape->Shadow->PutOffsetX(0.f);
// // 						spShape->Shadow->PutOffsetY(2.5f);
// 						spShape->PutHeight(uniques[j][k] == 1 ? 50.f : hCrenaux / uniques[j][k]);
// 						spShape->PutWidth(lCrenaux * duree(jours[j][k]));

						margeGaucheTot += lCrenaux * dureeC;
// 						n++;
// 						if (n == uniques[j][k])
// 							n = 0;
					}
				}
				ridx++;
				if (ridx > 6)
					ridx = 0;
			}
			cout << "Diaporama des " << page << "A g\202n\202r\202" << endl;
		}


		/////////////////////////////////////////////////////////////////////
		// Sauvegarde du fichier
		// 
		cout << "Sauvegarde du fichier" << endl;

		spPre->SaveAs(_bstr_t(fichierDiapo), 
			PowerPoint::ppSaveAsOpenXMLPresentation, Office::msoTriStateMixed);

		if (!proxyUL)
			cin.ignore();

		// Lance le diaporama
//		spPre->SlideShowSettings->Run();

		// Fermeture du diaporama
//      spPre->Close();


		/////////////////////////////////////////////////////////////////////
		// Fermer PowerPoint.
		// 
// 		cout << "Fermeture de PowerPoint" << endl;
// 		spPpApp->Quit();

	}
	catch (_com_error &err)
	{
		wprintf(L"PowerPoint throws the error: %s\n", err.ErrorMessage());
		wprintf(L"Description: %s\n", (LPCWSTR) err.Description());
		cin.ignore();
	}

	// Fin COM pour ce thread
	CoUninitialize();

	return 0;
}